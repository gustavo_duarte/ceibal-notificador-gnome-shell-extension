//Modified Hello-World to launch a new Terminal instead of showing Hello World
const St = imports.gi.St;
const Main = imports.ui.main;
const Shell = imports.gi.Shell;

let app, button, icon;

function _startTerminal() {
    if(!app) {
        app = Shell.AppSystem.get_default().lookup_app('ceibal-notificador-extension.desktop');
    }
    app.open_new_window(-1);
    Main.overview.hide();
}

function init() {
    button = new St.Bin({ style_class: 'panel-button',
                          reactive: true,
                          can_focus: true,
                          x_fill: true,
                          y_fill: false,
                          track_hover: true });
    icon = new St.Icon({ style_class: "icon-Ceibal-info"});

    button.set_child(icon);
    button.connect('button-press-event', _startTerminal);
}

function enable() {
    Main.panel._rightBox.insert_child_at_index(button, 2);
}

function disable() {
    Main.panel._rightBox.remove_child(button);
}
