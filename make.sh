#!/bin/bash

install() {
    sudo mkdir -p "/usr/share/gnome-shell/extensions"
    sudo cp -R "ceibalnotificador@ceibal.edu.uy" "/usr/share/gnome-shell/extensions/"
    
    #sudo mkdir -p "/usr/share/glib-2.0/schemas"
    #sudo cp "org.gnome.shell.extensions.ceibalnotificador.gschema.xml" "/usr/share/glib-2.0/schemas/"
    #sudo glib-compile-schemas "/usr/share/glib-2.0/schemas"
    
    sudo mkdir -p "/usr/share/applications"
    sudo cp "ceibal-notificador-extension.desktop" "/usr/share/applications/"

    sudo mkdir -p "/usr/share/icons"
    sudo cp "ceibal-notificador-extension.svg" "/usr/share/icons/"
    #gsettings set org.gnome.shell enabled-extensions "['ceibalnotificador@ceibal.edu.uy']"  
    
    echo "CeibalNotificador instalado correctamente"
}

uninstall() {
    sudo rm -R "/usr/share/gnome-shell/extensions/ceibalnotificador@ceibal.edu.uy"
    
    #sudo rm "/usr/share/glib-2.0/schemas/org.gnome.shell.extensions.ceibalnotificador.gschema.xml"
    #sudo glib-compile-schemas "/usr/share/glib-2.0/schemas"
    #dconf reset -f /org/gnome/shell/extensions/ceibalnotificador/

    sudo rm "/usr/share/applications/ceibal-notificador-extension.desktop"

    echo "CeibalNotificador desinstalado."
}

if [ $# -ne "1" ] || [ $1 = "help" ]; then
    echo "Usage:"
    echo " $0 install - Install timer-extension"
    echo " $0 uninstall - Uninstall timer-extension"
    echo " $0 help - Show this help"
    exit 0
fi

if [ $1 = "install" ]; then
    install
fi

if [ $1 = "uninstall" ]; then
    uninstall
fi
